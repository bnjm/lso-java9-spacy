#
# Copyright (c) 2016-2017, Leftshift One
# __________________
# [2017] Leftshift One
# All Rights Reserved.
# NOTICE:  All information contained herein is, and remains
# the property of Leftshift One and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Leftshift One
# and its suppliers and may be covered by Patents,
# patents in process, and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Leftshift One.

FROM openjdk:9-jdk-slim

ENV PATH /usr/local/bin:$PATH
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3 \
    python3-pip \
    build-essential \
    python3-dev \
    curl \
    git \
    wget \
    xvfb \
    zip \
    gnupg2 \
    && rm -rf /var/lib/apt/lists/*

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get install -y nodejs

RUN npm install -g yarn

RUN pip3 install --upgrade pip setuptools wheel
RUN pip3 install -U spacy --no-cache-dir && python3 -m spacy download de